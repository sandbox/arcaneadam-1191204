<?php
/**
 * @file Base hooks and functions needed the theme
 */

/**
 * Implements hook_theme()
 */
function puzzle_theme($registry, $type, $theme, $path) {
  //rewite the registry to for our themes implementation of dynamic theming
  puzzle_rebuild_theme_registry($registry, $type, $theme, $path);
  return $registry;
}

/**
 * Takes the current theme registry and rebuids it to point every function
 * that we override to our version.
 * 
 * For parameters see hook_theme()
 * @see hook_theme();
 */
function puzzle_rebuild_theme_registry(&$registry, $type, $theme, $path) {
  global $puzzle_theme_registry;
  $registry_process_functions = array();
  $processed_functions = array();
  $processed_preprocess_functions = array();
  $mask = $theme . '.*.inc';
  $nomask = array('.', '..', '.git', 'examples');
  $files = file_scan_directory($path, $mask);
  foreach ($files as $file) {
   $parts = explode('.', $file->name);
    $hook = $parts[1];
    if (isset($registry[$hook])) {
      //Since this function only runs on registry rebuilding we will load the file and check for 
      //the function override. If it exists we will alter the function and force this hook to include this file
      $function = $theme . '_' . $hook;
      include_once($file->filename);
      if (!isset($processed_functions[$function])) {
        if (function_exists($function) && empty($registry[$hook]['template'])) {
          //We need to make sure we don't accidentally overwrite a template implementation with a function
          $registry[$hook]['function'] = $function;
          $processed_functions[$function] = TRUE;
          $registry[$hook]['include files'][] = $file->filename;
        }
      }
      $preprocess_function = $theme . '_preprocess_' . $hook;
      if (!isset($processed_preprocess_functions[$preprocess_function])) {
        if (function_exists($preprocess_function)) {
          $registry[$hook]['preprocess functions'] = $preprocess_function;
          $processed_preprocess_functions[$function] = TRUE;
          $registry[$hook]['include files'][] = $file->filename;
        }
      }
      
    }
    //This is a special function with the name THEME_registry_process_HOOK
    //It allows a module to define a special function to process a registry hook
    //and add some special handling to the puzzle_theme_registry
    //For an example see the puzzle_registry_process_node and puzzle_preprocess_node
    $registry_process = $theme . '_' . 'registry_process' . '_' . $hook;
    if (!isset($registry_process_functions[$hook])) {
      if (function_exists($registry_process)) {
        $registry_process_functions[$hook] = array($registry_process);
      }
    }
  }
  //We are now going to iterate through the different registry process functions
  //Since the files they could be included in are already included above then we don't need
  //to load the file. In fact that file and function will only get called on when the Theme Cache is rebuilt
  $registry_process_theme = $theme . '_' . 'registry_process';
  if (function_exists($registry_process_theme)) {
    call_user_func_array($registry_process_theme, array($puzzle_theme_registry));
  }
  foreach($registry_process_functions as $hook => $function) {
    $args = array($puzzle_theme_registry, $registry[$hook]);
    call_user_func_array($function, $args);
  }
  cache_set('puzzle_theme_registry', $puzzle_theme_registry);
}

/**
 * Loads any include files in an array
 */
function puzzle_load_files_array($files) {
  if (is_array($files['include files'])) {
    foreach($files['include files'] as $file) {
      include_once($file);
    }
  }
}
/**
 * Initializes the theme by getting the puzzle registry.
 */
function puzzle_init() {
  global $puzzle_theme_registry;
  $puzzle_theme_registry = cache_get('puzzle_theme_registry');
}

puzzle_init();