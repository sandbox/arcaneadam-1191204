<?php
/*
 * @file An example implementaion of how to override theme_username using the puzzle theme
 * this file, puzzle.username.inc, is contained on it's own and is only called when needed
 */

/**
 * Format a username. This function is different from the default implementation in that
 * it replaces links to the user profiles with the homepage if present,
 * removes the "(not verified)" text, and changes the Anonymous label to 'Jon Doe'
 *
 * @param $object
 *   The user object to format, usually returned from user_load().
 * @return
 *   A string containing an HTML link to the user's homepage if the passed object
 *   contains this. Otherwise, only the username is returned or 'Jon Doe' for non register users.
 */
function puzzle_username($object) {
  if (!empty($object->name)) {
    $name = $object->name;
    if (drupal_strlen($name) > 20) {
      $name = drupal_substr($name, 0, 15) .'...';
    }
    if (!empty($object->homepage)) {
      $output = l($name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($name);
    }
  }
  else {
    $output = check_plain(t('Jon Doe'));
  }

  return $output;
}
