<?php
function puzzle_preprocess_node(&$vars) {
  global $theme, $puzzle_theme_registry;
  $node = $vars['node'];
  $args = func_get_args();
  if (isset($puzzle_theme_registry['node'][$node->type])) {
    puzzle_load_files_array($puzzle_theme_registry['node'][$node->type]);
    if (isset($puzzle_theme_registry['node'][$node->type]['preprocess functions']) && 
    is_array($puzzle_theme_registry['node'][$node->type]['preprocess functions'])) {
      foreach($puzzle_theme_registry['node'][$node->type]['preprocess functions'] as $function) {
        call_user_func_array($function, $args);
      }
    }
  }
  if (isset($puzzle_theme_registry['node'][$node->nid])) {
    puzzle_load_files_array($puzzle_theme_registry['node'][$node->nid]);
    if (isset($puzzle_theme_registry['node'][$node->nid]['preprocess functions']) && 
    is_array($puzzle_theme_registry['node'][$node->nid]['preprocess functions'])) {
      foreach($puzzle_theme_registry['node'][$node->nid]['preprocess functions'] as $function) {
        call_user_func_array($function, $args);
      }
    }
  }
}
