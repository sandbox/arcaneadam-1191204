<?php
/**
 * @file Process the puzzle_theme_registry for the node hook
 */
function puzzle_registry_process_node(&$registry) {
  //Node is a special case since it can use types and we want to break down
  //the preprocess functions to include puzzle_preprocess_node_NODETYPE or preprocess_node_NID
  //Unfortuneately there is not a way to do this with in the theme('node') functionality
  //So we must instead use our preprocess_node function to load these, but in order to not be
  //making a file_scan_directory on every preprocess node call we will use a theme cache to save
  //preprocess_node_NODETYPE functions for each theme. This makes it easy to grab this info and
  //reuse it in a non memory intensive way.
  if (isset($parts[2]) && isset($parts[3])) {
    //parts[2] would be preprocess and parts[3] would be the node->type or node->nid
    //We've already included the file so now we simple check to make sure the function exists
    $type = $parts[3];
    $node_preprocess_function =  $theme . '_preprocess_' . $hook . '_' . $type;
    if (function_exists($node_preprocess_function)) {
      $registry['node'][$type] = array(
        'include files' => array($file->filename),
        'preprocess functions' => array($node_preprocess_function),
      );
    }
  }
}
